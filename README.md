# mail-campaign

Client side application is developed with React Js. Therefore npm and node.js must be installed to run this application.
To run the application npm start command must be called.
 - First of all file should be uploaded to add contacts in the application in the "File Upload" menu.
 - Then contacts can be shown, add and delete from the "Contacts" menu.
 - To send mail, select the contacts, type body, and click the Send Mail button from the "Mail" menu.
 - To display the results "Mail Report" menu can be clicked.


