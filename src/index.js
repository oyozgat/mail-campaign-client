import React from "react";
import {render} from "react-dom";
import {Router, Route, browserHistory, IndexRoute} from "react-router";

import {Root} from "./components/Root";
import {Home} from "./components/Home";

import {FileUpload} from "./components/FileUpload";
import {Contacts} from "./components/Contacts";
import {Mail} from "./components/Mail";
import {MailReport} from "./components/MailReport";
import {Success} from "./components/Success";

class App extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path={"/"} component={Root} >
                    <IndexRoute component={Home} />
                    <Route path={"home"} component={Home} />
                    <Route path={"fileUpload"} component={FileUpload} />
                    <Route path={"contact"} component={Contacts} />
                    <Route path={"mail"} component={Mail} />
                    <Route path={"mailReport"} component={MailReport} />
                    <Route path={"success/:key"} component={Success} />
                </Route>
                <Route path={"home-single"} component={Home}/>
            </Router>
        );
    }
}

render(<App />, window.document.getElementById('app'));