import React from "react";
import {BootstrapTable} from "react-bootstrap-table";
import {TableHeaderColumn} from "react-bootstrap-table";
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

const selectRow = {
    mode: 'checkbox',
    bgColor: 'rgb(238, 193, 213)'
};

function onAfterInsertRow(row) {
    fetch('http://localhost:8080/contact/save', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(row)
    }).then(data => data.json())
        .then(data => {
        row = data;
    }).catch(reason => console.log(reason));
}

function onAfterDeleteRow(row){
    let contact = { id: row[0]};
    fetch('http://localhost:8080/contact/delete', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(contact)
    }).catch(reason => console.log(reason));
}

const options = {
    afterInsertRow: onAfterInsertRow,   // A hook for after insert rows
    afterDeleteRow: onAfterDeleteRow
};

export class ContactTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {contacts: []};
    }
    componentDidMount() {
        fetch('http://localhost:8080/contact/getAll')
            .then(data => data.json())
            .then((data) => { this.setState({ contacts: data }) });
    }

    render() {
        return (
            <BootstrapTable data={this.state.contacts} selectRow={selectRow} insertRow={true} deleteRow={true} options={options}>
                <TableHeaderColumn dataField='id' hidden = {true} isKey={ true }>Contact ID</TableHeaderColumn>
                <TableHeaderColumn dataField='fullName' editable={ { type: 'textarea' } }>Full Name</TableHeaderColumn>
                <TableHeaderColumn dataField='mailAddress' editable={ { type: 'textarea' } }>Mail Address</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}