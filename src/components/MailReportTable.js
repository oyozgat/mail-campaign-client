import React from "react";
import {BootstrapTable} from "react-bootstrap-table";
import {TableHeaderColumn} from "react-bootstrap-table";

const selectRow = {
    mode: 'checkbox',
    bgColor: 'rgb(238, 193, 213)'
};

function timeFormatter(time){
    if(time === 0){
        return "-";
    }else {
        return new Date(time).toLocaleString();
    }
}

export class MailReportTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {mailReport: []};
    }
    componentDidMount() {
        fetch('http://localhost:8080/mail/getAllReports')
            .then(data => data.json())
            .then((data) => { this.setState({ mailReport: data }) });
    }

    render() {
        return (
            <BootstrapTable data={this.state.mailReport} selectRow={selectRow} insertRow={false} deleteRow={false}>
                <TableHeaderColumn dataField='id' hidden = {true} isKey={ true }>Report ID</TableHeaderColumn>
                <TableHeaderColumn dataField='mailAddress' editable={ { type: 'textarea' } }>Mail Address</TableHeaderColumn>
                <TableHeaderColumn dataField='sentTime' dataFormat={timeFormatter} editable={ { type: 'textarea' } }>Mail Received Time</TableHeaderColumn>
                <TableHeaderColumn dataField='clickedTime' dataFormat={timeFormatter} editable={ { type: 'textarea' } }>Mail Clicked Time</TableHeaderColumn>
                <TableHeaderColumn dataField='timeDiff' editable={ { type: 'textarea' } }>Time Difference</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}