import React from "react";
import {MailReportTable} from "./MailReportTable";

export class MailReport extends React.Component {
    render() {
        return (
            <div>
                <h3>Mail Report</h3>
                <div>
                    <MailReportTable/>
                </div>
            </div>
        );
    }
}