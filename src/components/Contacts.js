import React from "react";
import {ContactTable} from "./ContactTable";

export class Contacts extends React.Component {
    render() {
        return (
            <div>
                <h3>Contacts</h3>
                <div>
                    <ContactTable/>
                </div>
            </div>
        );
    }
}