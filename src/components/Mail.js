//import 'bootstrap/dist/css/bootstrap.min.css'
import React from "react";
import FilteredMultiSelect from "react-filtered-multiselect"
import axios from "axios";

const BOOTSTRAP_CLASSES = {
  filter: 'form-control',
  select: 'form-control',
  button: 'btn btn btn-block btn-default',
  buttonActive: 'btn btn btn-block btn-primary',
}

export class Mail extends React.Component {

    state = {
        selectedOptions: [],
        contacts: [],
        mailSentResult:'',
        value: ''
      }

     componentDidMount() {
         fetch('http://localhost:8080/contact/getAll')
                .then(data => data.json())
                .then((data) => { this.setState({ contacts: data }) });
      }

     handleDeselect(index) {
       var selectedOptions = this.state.selectedOptions.slice()
       selectedOptions.splice(index, 1)
       this.setState({selectedOptions})
     }

     handleClearSelection = (e) => {
       this.setState({selectedOptions: []})
     }

     handleSelectionChange = (selectedOptions) => {
       selectedOptions.sort((a, b) => a.id - b.id)
       this.setState({selectedOptions})
     }

     handleChange = (event) => {
        this.setState({value: event.target.value})
     }

     sendMail() {
       var textAreaValue = this.state.value;
       var selectedOptions = this.state.selectedOptions.slice();
       const url = "http://localhost:8080/mail/send";
       const campaign = {
          name: 'Test Campaign',
          body: textAreaValue,
          contactList: selectedOptions
       };
       axios.post(url, campaign).then(response => {
           console.log("Mail is sent");
           this.setState({mailSentResult: 'Mail is sent successfully'});
       })
     }

     render() {
         var {selectedOptions} = this.state
         var {contacts} = this.state
         return <div className="row">
           <div className="row">
           <div className="col-md-5">
             <FilteredMultiSelect
               classNames={BOOTSTRAP_CLASSES}
               onChange={this.handleSelectionChange}
               options={contacts}
               selectedOptions={selectedOptions}
               textProp="fullName"
               valueProp="id"
             />
             <p className="help-block">Press select contacts to send mail</p>
           </div>
           <div className="col-md-5">
             {selectedOptions.length === 0 && <p>(nothing selected yet)</p>}
             {selectedOptions.length > 0 && <ol>
               {selectedOptions.map((ship, i) => <li key={ship.id}>
                 {`${ship.fullName} <${ship.mailAddress}> `}
                 <span style={{cursor: 'pointer'}} onClick={() => this.handleDeselect(i)}>
                   &times;
                 </span>
               </li>)}
             </ol>}
             {selectedOptions.length > 0 && <button style={{marginLeft: 20}} className="btn btn-default" onClick={this.handleClearSelection}>
               Clear Selection
             </button>}
           </div>
           </div>
           <div className="row">
              <div className="col-md-10">
                 <textarea  onChange={this.handleChange} style={{width: 'inherit', height:'inherit'}}/>
              </div>
           </div>
           <div className="row">
                <button style={{marginLeft: 20}} className="btn btn-default" onClick={() => this.sendMail()}>Send Mail</button>
           </div>
           <div className="row">
                <div>{this.state.mailSentResult}</div>
           </div>
         </div>
     }
}