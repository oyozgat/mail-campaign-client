import React from "react";
import axios from "axios";

export class FileUpload extends React.Component {

    constructor(props) {
            super(props);
            this.state = {uploadMsg: ""};
    }

    onChange(e) {
         const url = "http://localhost:8080/file/upload";
         e.preventDefault();
         const formData = new FormData();
         formData.append('file', e.target.files[0]);
         return axios.post(url, formData).then(response => {
            this.setState({ uploadMsg: "File is uploaded successfully. " + JSON.stringify(response.data.successCount) +
            " contact saved successfully. " + JSON.stringify(response.data.failCount) + " failed while saving."});
           }
         );
    }

    render() {
        return (
            <div>
                <h3>File Upload</h3>
                <div onSubmit={this.onFormSubmit}>
                    <input type="file" name="file" onChange={(e)=>this.onChange(e)}/>
                </div>
                <div>{this.state.uploadMsg}</div>
            </div>
        );
    }
}