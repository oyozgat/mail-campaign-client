import React from "react";
import axios from "axios";

export class Success extends React.Component {

    componentDidMount() {
         const url = "http://localhost:8080/mail/acknowledge";
         const key = this.props.params.key;
         axios.post(url, key, {
           headers: { 'Content-Type': 'text/plain' }
         });
    }

    render() {
        return (
            <div>
                <h3>Success</h3>
                <h2>Congratulations!</h2>
                <p>You have successfully registered to the campaign.</p>
            </div>
        );
    }
}