import React from "react";
import {Link} from "react-router";

export const Header = (props) => {
    return (
        <nav className="navbar navbar-default">
            <div className="container">
                <div className="navbar-header">
                    <ul className="nav navbar-nav">
                        <li><Link to={"/home"} activeStyle={{color: "red"}}>Home</Link></li>
                        <li><Link to={"/fileUpload"} activeStyle={{color: "red"}}>File Upload</Link></li>
                        <li><Link to={"/contact"} activeStyle={{color: "red"}}>Contacts</Link></li>
                        <li><Link to={"/mail"} activeStyle={{color: "red"}}>Mail</Link></li>
                        <li><Link to={"/mailReport"} activeStyle={{color: "red"}}>Mail Report</Link></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};